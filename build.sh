#!/bin/bash
set -e
echo "Building Example.java..."
javac -cp "lib/sp-core.jar:lib/sp-tty.jar" Example.java
echo "Build completed successfully"
